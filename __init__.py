from .nbg import (
				__author__,
				_data,
				_xml,
				_nbgtree,
				_nbgroot,
				NBGParser,
	)