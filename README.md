# ngb - National Bank of Georgia
 * Lari Exchange Rates

Installation Requirements
-----------------------------------

* Python 3.0, 3.1, 3.2, 3.3, 3.4, 3.5
* Django >= 1.7

Using a very simple:

	$ git clone https://github.com/Lh4cKg/nbg
	$ cd nbg
	$ python nbg.py

see below screen

[![Py-NBG](https://github.com/Lh4cKg/nbg/blob/master/img/expy.png)]()

Using the example of Django

create a simple function in views.py

	from .nbg import NBGParser

	def nbg(request):
		rc = RequestContext(request)
		c = {}
		_ins = NBGParser()
		rates = _ins.desc
		c['nbg'] = rates
		return render_to_response('nbg.html',c,rc)

create nbg.html in templates
	
	<!DOCTYPE html>
	<html>
	<head>
		<title>nbg</title>
	</head>
	<body>
	{% if nbg %}
		
		{{nbg|safe|slice:"6521:6680"}}<br/>
		{{nbg|safe|slice:"2150:2300"}}<br/>
		{{nbg|safe|slice:"2300:2480"}}

	{% endif %}
	</body>
	</html>

see below screen

[![Py-NBG](https://github.com/Lh4cKg/nbg/blob/master/img/exdjango.png)]()
