#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "Lasha Gogua"

"""
Lari Exchange Rates package for National Bank Of Georgia
NBG official web site - https://www.nbg.gov.ge
"""

# python imports
from urllib.request import urlopen
import xml.etree.ElementTree as ET
from html.parser import HTMLParser

NBG_URL = 'nbg.gov.ge/rss.php'

class NBGParser(HTMLParser):
	def __init__(self):
		HTMLParser.__init__(self)
		self.url = NBG_URL
		self.recording = 0
		self.data = []

	def get_xml_data(self):
		value = None
		_url = urlopen('https://%s' % self.url)
		if _url.status == 200:
			data = _url
			if data:
				value = data
		else:
			print("არ არის სწორი მონაცემები")
		return value

	@property
	def nbgxml(self):
		xml = self.get_xml_data()
		return xml

	@property
	def nbgtree(self):
		tree = ET.parse(self.nbgxml)
		return tree

	@property
	def nbgroot(self):
		root = self.nbgtree.getroot()
		return root

	@property
	def desc(self):
		_desc = [d for d in self.nbgroot.iter('description')]
		# data = [i for i in _desc[1].itertext()]
		for data in _desc[1].itertext():
			print()
		return data

	def handle_starttag(self, tag, attributes):
		if tag != 'tr':
			return
		if self.recording:
			self.recording += 1
			return
		self.recording = 1

	def handle_endtag(self, tag):
		if tag == 'tr' and self.recording:
			self.recording -= 1

	def handle_data(self, data):
		if self.recording == True:
			print(data,end="")

_inst_nbgparser = NBGParser()
_data = _inst_nbgparser.desc
_xml = _inst_nbgparser.nbgxml
_nbgtree = _inst_nbgparser.nbgtree
_nbgroot = _inst_nbgparser.nbgroot

def main():
	_data = _inst_nbgparser.desc
	_feed = _inst_nbgparser.feed(str(_data.rstrip('\n')))
	print(_feed)

if __name__ == "__main__":
	main()